package business;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing{
    private ArrayList<MenuItem> items;
    private HashMap<Order, ArrayList<MenuItem>> orders;
    private ArrayList<Order> keys;
    private ArrayList<Client> clients;

    public DeliveryService(){
        items = new ArrayList<>();
        orders = new HashMap<>();
        keys = new ArrayList<>();
        clients = new ArrayList<>();
        clients.add(new Client(0,"name","asd"));
    }

    boolean wellFormed(){
        return items != null && orders != null && keys != null && clients != null;
    }

    public void addItemToOrder(int clientId, String name){
        assert name != null;
        assert wellFormed();
        for(Client c : clients){
            if(c.getClientId() == clientId) {
                for(MenuItem i : items){
                    if(i.getTitle().contains(name)) {
                        c.addToList(i);
                        break;
                    }
                }
                break;
            }
        }
    }

    public void placeOrder(int clientId){
        assert clientId <= clients.size();
        assert wellFormed();
        int orderId, price = 0;
        if(keys.isEmpty())
            orderId = 0;
        else
            orderId = keys.size();
        Client client = null;
        for(Client i : clients){
            if(i.getClientId() == clientId) {
                client = i;
                break;
            }
        }
        if(client != null) {
            ArrayList<MenuItem> list = client.getList();
            for(MenuItem j : list){
                price += j.getPrice();
                j.incrementNoOfOrders();
            }
            Order order = new Order(orderId,clientId,price);
            keys.add(order);
            orders.put(order, client.getList());
        }
    }

    public void importProducts(){
        assert items.size() == 0;
        assert wellFormed();
        int x = 0;
        List<BaseProduct> products = new ArrayList<>();
        try {
            Stream<String> s = Files.lines(Paths.get("products.csv"));
            products = s
                    .skip(1)
                    .map(DeliveryService::createProduct)
                    .collect(Collectors.toList());
        } catch(IOException e){
            e.printStackTrace();
        }
        for(BaseProduct bp: products){
            x = 0;
            for(MenuItem p: items){
                if(bp.getTitle().equals(p.getTitle())){
                    x++;
                }
            }
            if(x < 1)
                items.add(bp);
        }
        assert items.size() != 0;
    }

    private static BaseProduct createProduct(String line){
        List<String> st = Arrays.asList(line.split(","));
        BaseProduct product = new BaseProduct(st.get(0).trim(), Double.parseDouble(st.get(1)), Integer.parseInt(st.get(2)), Integer.parseInt(st.get(3)), Integer.parseInt(st.get(4)), Integer.parseInt(st.get(5)), Integer.parseInt(st.get(6)));
        return product;
    }

    public ArrayList<MenuItem> findProducts(String title, String rating, String calories, String protein, String fat, String sodium, String price){
        assert title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null;
        assert wellFormed();
        List<MenuItem> list = items;
        if(!title.equals(""))
            list = list.stream().filter(i -> i.getTitle().contains(title)).collect(Collectors.toList());
        try {
            if (!rating.equals(""))
                list = list.stream().filter(i -> i.getRating() >= Double.parseDouble(rating)).collect(Collectors.toList());
            if (!calories.equals(""))
                list = list.stream().filter(i -> i.getCalories() <= Integer.parseInt(calories)).collect(Collectors.toList());
            if (!protein.equals(""))
                list = list.stream().filter(i -> i.getProtein() == Integer.parseInt(protein)).collect(Collectors.toList());
            if (!fat.equals(""))
                list = list.stream().filter(i -> i.getFat() == Integer.parseInt(fat)).collect(Collectors.toList());
            if (!sodium.equals(""))
                list = list.stream().filter(i -> i.getSodium() == Integer.parseInt(sodium)).collect(Collectors.toList());
            if (!price.equals(""))
                list = list.stream().filter(i -> i.getPrice() <= Integer.parseInt(price)).collect(Collectors.toList());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        assert list != null;
        return (ArrayList<MenuItem>)list;
    }

    public void addProduct(String title, String rating, String calories, String protein, String fat, String sodium, String price){
        assert title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null;
        assert wellFormed();
        BaseProduct product;
        try {
            product = new BaseProduct(title, Double.parseDouble(rating), Integer.parseInt(calories), Integer.parseInt(protein), Integer.parseInt(fat), Integer.parseInt(sodium), Integer.parseInt(price));
        } catch(NumberFormatException s){
            return;
        }
        items.add(product);
    }

    public void modifyProduct(String title, String rating, String calories, String protein, String fat, String sodium, String price){
        assert title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null;
        assert wellFormed();
        for(MenuItem i : items) {
            if (i.getTitle().equals(title)) {
                try {
                    if (!rating.equals(""))
                        i.setRating(Double.parseDouble(rating));
                    if (!calories.equals(""))
                        i.setCalories(Integer.parseInt(calories));
                    if (!protein.equals(""))
                        i.setCalories(Integer.parseInt(protein));
                    if (!fat.equals(""))
                        i.setCalories(Integer.parseInt(fat));
                    if (!sodium.equals(""))
                        i.setCalories(Integer.parseInt(sodium));
                    if (!price.equals(""))
                        i.setCalories(Integer.parseInt(price));
                } catch (NumberFormatException s) {
                    return;
                }
                return;
            }
        }
    }

    public void deleteProduct(String title){
        assert title != null;
        assert wellFormed();
        int c = 0;
        for(MenuItem i : items) {
            if (title.equals(i.getTitle())) {
                items.remove(c);
                return;
            }
            c++;
        }
    }

    public void addToCompositeProduct(String cName, String iName){
        assert cName != null && iName != null;
        assert wellFormed();
        BaseProduct b = null;
        for(MenuItem j : items){
            if(j.getTitle().equals(iName) && j instanceof BaseProduct){
                b = (BaseProduct) j;
            }
        }
        if(b == null)
            return;
        for(MenuItem i : items){
            if(i.getTitle().equals(cName) && i instanceof CompositeProduct){
                ((CompositeProduct) i).addProduct(b);
                return;
            }
        }
        CompositeProduct cp = new CompositeProduct(cName);
        cp.addProduct(b);
        items.add(cp);
    }

    public void addClient(String name, String password){
        assert name != null && password != null;
        assert wellFormed();
        int clientId = clients.size();
        clients.add(new Client(clientId,name,password));
    }



    public Client getClient(int clientId){
        Client client = null;
        for(Client i : clients) {
            if(i.getClientId() == clientId)
                client = i;
        }
        return client;
    }
    public Order getOrder(int clientId){
        Order order = null;
        for(Order i : keys) {
            if(i.getClientId() == clientId)
                order = i;
        }
        return order;
    }
    public int getClientId(String name, String password){
        for(Client i : clients){
            if(i.getName().equals(name) && i.getPassword().equals(password)){
                return i.getClientId();
            }
        }
        return -1;
    }

    public ArrayList<MenuItem> getItems(){
        return items;
    }
    public ArrayList<MenuItem> getOrderedItems(Order order){
        return orders.get(order);
    }
    public ArrayList<Client> getClients(){
        return clients;
    }
    public ArrayList<Order> getKeys(){
        return keys;
    }
}
