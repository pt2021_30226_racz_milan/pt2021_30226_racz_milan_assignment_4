package business;

import java.util.ArrayList;

public class Client implements java.io.Serializable{
    private int clientId;
    private String name;
    private String password;
    private ArrayList<MenuItem> list;

    public Client(int clientId, String name, String password){
        this.clientId = clientId;
        this.name = name;
        this.password = password;
        list = new ArrayList<>();
    }

    public int getPrice(){
        int price = 0;
        for(MenuItem i : list){
            price += i.getPrice();
        }
        return price;
    }

    public void newOrderList(){
        list = new ArrayList<>();
    }
    public void addToList(MenuItem item){
        list.add(item);
    }

    public ArrayList<MenuItem> getList(){
        return list;
    }
    public int getClientId(){
        return clientId;
    }
    public String getName(){
        return name;
    }
    public String getPassword(){
        return password;
    }

}
