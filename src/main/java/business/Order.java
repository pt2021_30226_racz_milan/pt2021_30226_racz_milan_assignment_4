package business;

import java.time.LocalDateTime;

public class Order implements java.io.Serializable{
    private int orderId;
    private int clientId;
    private int price;
    private LocalDateTime date;

    public Order(int orderId, int clientId, int price){
        this.orderId = orderId;
        this.clientId = clientId;
        this.price = price;
        this.date = LocalDateTime.now();
    }

    @Override
    public int hashCode(){
        return orderId;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Order))
            return false;
        Order n = (Order) o;
        boolean orderIdEquals = (this.orderId == n.orderId);
        boolean clientIdEquals = (this.clientId == n.clientId);
        boolean priceEquals = (this.price == n.price);
        boolean dateEquals = (this.date == null && n.date == null) || (this.date != null && this.date.equals(n.date));
        return orderIdEquals && clientIdEquals && priceEquals && dateEquals;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getOrderId(){
        return orderId;
    }
    public int getClientId(){
        return clientId;
    }
    public int getPrice(){
        return price;
    }
    public LocalDateTime getDate(){
        return date;
    }
}
