package business;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem{

    private ArrayList<MenuItem> items;
    private int price;

    public CompositeProduct(String title){
        super(title);
        items = new ArrayList<>();
    }

    public void addProduct(BaseProduct pr){
        items.add(pr);
        price = computePrice();
    }

    public int computePrice() {
        int pr = 0;
        for(MenuItem i : items){
            pr += i.getPrice();
        }
        return pr;
    }

    public double getRating() {
        return 0;
    }
    public int getCalories() {
        return 0;
    }
    public int getProtein() {
        return 0;
    }
    public int getFat() {
        return 0;
    }
    public int getSodium() {
        return 0;
    }
    public int getPrice() {
        return price;
    }

    public void setRating(double rating) {
        return;
    }
    public void setCalories(int calories) {
        return;
    }
    public void setProtein(int protein) {
        return;
    }
    public void setFat(int fat) {
        return;
    }
    public void setSodium(int sodium) {
        return;
    }
    public void setPrice(int price) {
        return;
    }
}
