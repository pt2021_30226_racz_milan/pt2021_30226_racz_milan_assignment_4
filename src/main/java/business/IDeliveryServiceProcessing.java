package business;

import java.util.ArrayList;

public interface IDeliveryServiceProcessing {

    /**
     * Adds an item to the client's list.
     * @param clientId
     * @param name
     * @pre name != null
     * @invariant wellFormed()
     */
    void addItemToOrder(int clientId, String name);

    /**
     * Creates an order.
     * @param clientId
     * @pre clientId <= clients.size()
     * @invariant wellFormed()
     */
    void placeOrder(int clientId);

    /**
     * Imports the products from products.csv.
     * @pre items.size() == 0
     * @post items.size() != 0
     * @invariant wellFormed()
     */
    void importProducts();

    /**
     * Searches for products based on one or multiple criteria.
     * @param title
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @return
     * @pre title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null
     * @post list != null
     * @invariant wellFormed()
     */
    ArrayList<MenuItem> findProducts(String title, String rating, String calories, String protein, String fat, String sodium, String price);

    /**
     * Creates and adds a new product to the items list.
     * @param title
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @pre title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null
     * @invariant wellFormed()
     */
    void addProduct(String title, String rating, String calories, String protein, String fat, String sodium, String price);

    /**
     * Modifies an existing product.
     * @param title
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     * @pre title != null && rating != null && calories != null && protein != null && fat != null && sodium != null && price != null
     * @invariant wellFormed()
     */
    void modifyProduct(String title, String rating, String calories, String protein, String fat, String sodium, String price);

    /**
     * Deletes an existing product from the items list.
     * @param title
     * @pre title != null
     * @invariant wellFormed()
     */
    void deleteProduct(String title);

    /**
     * Adds a base product to a composite product and creates a new composite product if it does not exist.
     * @param cName
     * @param iName
     * @pre cName != null && iName != null
     * @invariant wellFormed()
     */
    void addToCompositeProduct(String cName, String iName);

    /**
     * Creates and adds a new client to the clients list.
     * @param name
     * @param password
     * @pre name != null && password != null
     * @invariant wellFormed()
     */
     void addClient(String name, String password);

}

