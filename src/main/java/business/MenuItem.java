package business;

public abstract class MenuItem implements java.io.Serializable{
    private String title;
    private int noOfOrders = 0;

    public MenuItem(String title){
        this.title = title;
    }

    public void incrementNoOfOrders(){
        noOfOrders++;
    };
    public abstract int computePrice();

    public int getNoOfOrders(){
        return noOfOrders;
    };
    public String getTitle(){
        return title;
    }
    public abstract double getRating();
    public abstract int getCalories();
    public abstract int getProtein();
    public abstract int getFat();
    public abstract int getSodium();
    public abstract int getPrice();

    public void setTitle(String title){
        this.title = title;
    }
    public abstract void setRating(double rating);
    public abstract void setCalories(int calories);
    public abstract void setProtein(int protein);
    public abstract void setFat(int fat);
    public abstract void setSodium(int sodium);
    public abstract void setPrice(int price);
}
