package data;

import business.Client;
import business.MenuItem;
import business.Order;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FileWriter {
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    public static void createBill(Order order, ArrayList<MenuItem> items){
        try {
            File file = new File("bill.txt");
            file.createNewFile();
            java.io.FileWriter f = new java.io.FileWriter("bill.txt",false);
            f.write("Order id: " + order.getOrderId() + "\n");
            f.write("Client id: " + order.getClientId() + "\n");
            f.write("Order list: " + "\n");
            for(MenuItem i : items){
                f.write(i.getTitle() + "\n");
            }
            f.write("Date: " + dtf.format(order.getDate()) + "\n");
            f.write("Total: " + order.getPrice() + "\n");
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public static void createReport1(ArrayList<Order> orders, int time1, int time2){
        try {
            File file = new File("report1.txt");
            file.createNewFile();
            java.io.FileWriter f = new java.io.FileWriter("report1.txt",false);
            f.write("The following orders have been performed between " + time1 + " and " + time2 + ": ");
            for(Order i : orders){
                f.write(i.getOrderId() + "; ");
            }
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public static void createReport2(ArrayList<MenuItem> items, int nr){
        try {
            File file = new File("report2.txt");
            file.createNewFile();
            java.io.FileWriter f = new java.io.FileWriter("report2.txt",false);
            f.write("The following products have been ordered more than " + nr + " times:\n");
            for(MenuItem i : items){
                f.write(i.getTitle() + ";\n");
            }
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public static void createReport3(ArrayList<Client> clients, int minOrders, int minPrice){
        try {
            File file = new File("report3.txt");
            file.createNewFile();
            java.io.FileWriter f = new java.io.FileWriter("report3.txt",false);
            f.write("The following clients have ordered more than " + minOrders + " times when the value of the order was more than " + minPrice + ": ");
            if (clients != null) {
                for (Client i : clients) {
                    f.write(i.getClientId() + "; ");
                }
            }
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public static void createReport4(ArrayList<MenuItem> items, int day){
        try {
            File file = new File("report4.txt");
            file.createNewFile();
            java.io.FileWriter f = new java.io.FileWriter("report4.txt",false);
            f.write("The products ordered within day "+ day + " with the number of times they have been ordered:\n");
            ArrayList<MenuItem> l = new ArrayList<>();
            if(items != null) {
                for (MenuItem i : items) {
                    int nr = 0;
                    boolean x = true;
                    for (MenuItem j : items) {
                        if (i.equals(j))
                            nr++;
                    }
                    for (MenuItem j : l) {
                        if (i.equals(j)) {
                            x = false;
                            break;
                        }
                    }
                    if (x)
                        f.write(i.getTitle() + " - " + nr + ";\n");
                    l.add(i);
                }
            }
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }
}
