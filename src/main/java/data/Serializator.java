package data;

import business.DeliveryService;

import java.io.*;

public class Serializator {

    public static void serializator(DeliveryService d){
        try{
            FileOutputStream f = new FileOutputStream("file.txt");
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(d);
            o.close();
            f.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static DeliveryService deserializator(){
        DeliveryService d = null;
        try{
            FileInputStream f = new FileInputStream("file.txt");
            ObjectInputStream i = new ObjectInputStream(f);
            d = (DeliveryService) i.readObject();
            i.close();
            f.close();
        } catch(IOException e) {
            e.printStackTrace();
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
            return (new DeliveryService());
        }
        return d;
    }
}
