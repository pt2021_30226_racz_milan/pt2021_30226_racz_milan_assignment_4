package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class EmployeeView implements Observer{

    private final JLabel l1 = new JLabel ("Orders: ");
    private JTextArea tf1 = new JTextArea(1,10);

    public EmployeeView(){
        JFrame frame = new JFrame ("Orders");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 250);

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JScrollPane scroll = new JScrollPane(tf1);

        FlowLayout flLayout = new FlowLayout();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(flLayout);

        panel.add(l1);
        panel.add(scroll);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public void updateList(ArrayList<MenuItem> items, int orderId){
        String str = "Order " + orderId + ": ";
        for(MenuItem i : items){
            str += "\t" + i.getTitle() + " \n";
        }
        String s = tf1.getText();
        s += str;
        tf1.setText(s);
    }

    public void show(ArrayList<MenuItem> items, int clientId, int orderId){
        String str = "Client with id " + clientId + " has ordered the products: \n";
        for(MenuItem i : items){
            str += "\t" + i.getTitle() + "\n";
        }
        JFrame frame = new JFrame("New order");
        frame.setSize(500,250);
        JTextArea ta = new JTextArea();
        ta.setText(str);
        JScrollPane s = new JScrollPane(ta);
        frame.add(s);
        frame.setVisible(true);
        updateList(items, orderId);
    }
}
