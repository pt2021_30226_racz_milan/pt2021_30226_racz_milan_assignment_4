package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientView {
    private final JLabel l1 = new JLabel ("Client operations: ");
    private final JLabel l2 = new JLabel ("Title: ");
    private final JLabel l3 = new JLabel ("Rating: ");
    private final JLabel l4 = new JLabel ("Calories: ");
    private final JLabel l5 = new JLabel ("Proteins: ");
    private final JLabel l6 = new JLabel ("Fats: ");
    private final JLabel l7 = new JLabel ("Sodium: ");
    private final JLabel l8 = new JLabel ("Price: ");
    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);
    private JTextArea tf3 = new JTextArea(1,10);
    private JTextArea tf4 = new JTextArea(1,10);
    private JTextArea tf5 = new JTextArea(1,10);
    private JTextArea tf6 = new JTextArea(1,10);
    private JTextArea tf7 = new JTextArea(1,10);
    private JTextArea tf8 = new JTextArea(10,20);
    private JButton b1 = new JButton("Search");
    private JButton b2 = new JButton("Add product");
    private JButton b3 = new JButton("Order");
    private JButton b4 = new JButton("View products");

    public ClientView(){
        JFrame frame = new JFrame ("Client operations");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 400);

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();

        JScrollPane scroll = new JScrollPane(tf8);

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(0,1,0,2);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(flLayout);
        panel2.setLayout(grLayout);
        panel3.setLayout(grLayout);
        panel4.setLayout(flLayout);
        panel5.setLayout(flLayout);

        panel1.add(l1);

        panel2.add(l2);
        panel2.add(l3);
        panel2.add(l4);
        panel2.add(l5);
        panel2.add(l6);
        panel2.add(l7);
        panel2.add(l8);

        panel3.add(tf1);
        panel3.add(tf2);
        panel3.add(tf3);
        panel3.add(tf4);
        panel3.add(tf5);
        panel3.add(tf6);
        panel3.add(tf7);

        panel4.add(panel2);
        panel4.add(panel3);

        panel5.add(b1);
        panel5.add(b2);
        panel5.add(b3);
        panel5.add(b4);

        panel.add(panel1);
        panel.add(panel4);
        panel.add(panel5);
        panel.add(scroll);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }
    public String getThird() {
        return tf3.getText();
    }
    public String getFourth() {
        return tf4.getText();
    }
    public String getFifth() {
        return tf5.getText();
    }
    public String getSixth() {
        return tf6.getText();
    }
    public String getSeventh() {
        return tf7.getText();
    }

    public void setEighth(ArrayList<MenuItem> items){
        String str = "";
        for(MenuItem i : items){
            str += i.getTitle() + "\n";
        }
        tf8.setText(str);
    }

    public void showProducts(ArrayList<MenuItem> items){
        String str = "";
        for(MenuItem i : items){
            str += i.getTitle() + "\n";
        }
        JFrame frame = new JFrame();
        frame.setSize(600,500);
        JTextArea ta = new JTextArea();
        ta.setText(str);
        JScrollPane s = new JScrollPane(ta);
        frame.add(s);
        frame.setVisible(true);
    }

    void addB1Listener(ActionListener s) {
        b1.addActionListener(s);
    }
    void addB2Listener(ActionListener s) {
        b2.addActionListener(s);
    }
    void addB3Listener(ActionListener s) {
        b3.addActionListener(s);
    }
    void addB4Listener(ActionListener s) {
        b4.addActionListener(s);
    }
}
