package presentation;

import business.*;
import data.FileWriter;
import data.Serializator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Controller {
    private View loginWindow;
    private AdministratorView adminWindow;
    private EmployeeView employeeWindow = null;
    private ClientView clientWindow;
    private DeliveryService d;
    private FileWriter fw;
    private int clientId;

    public Controller (){
        loginWindow = new View();
        d = Serializator.deserializator();
        if(d == null)
            d = new DeliveryService();
        fw = new FileWriter();

        loginWindow.addB1Listener(new AddLoB1Listener());
        loginWindow.addB2Listener(new AddLoB2Listener());
        loginWindow.addB3Listener(new AddLoB3Listener());
        loginWindow.addB4Listener(new AddLoB4Listener());
    }

    class AddLoB1Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if(loginWindow.getFirst().trim().equals("adm") && loginWindow.getSecond().trim().equals("123")) {
                adminWindow = new AdministratorView();

                adminWindow.addB1Listener(new AddAdB1Listener());
                adminWindow.addB2Listener(new AddAdB2Listener());
                adminWindow.addB3Listener(new AddAdB3Listener());
                adminWindow.addB4Listener(new AddAdB4Listener());
                adminWindow.addB5Listener(new AddAdB5Listener());
                adminWindow.addB6Listener(new AddAdB6Listener());
                adminWindow.addB7Listener(new AddAdB7Listener());
            }
        }
    }
    class AddLoB2Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            clientId = d.getClientId(loginWindow.getFirst().trim(),loginWindow.getSecond().trim());
            if(clientId != -1) {
                clientWindow = new ClientView();
                clientWindow.addB1Listener(new AddClB1Listener());
                clientWindow.addB2Listener(new AddClB2Listener());
                clientWindow.addB3Listener(new AddClB3Listener());
                clientWindow.addB4Listener(new AddClB4Listener());
            }
        }
    }
    class AddLoB3Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.addClient(loginWindow.getFirst().trim(),loginWindow.getSecond().trim());
        }
    }
    class AddLoB4Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if(loginWindow.getFirst().trim().equals("emp") && loginWindow.getSecond().trim().equals("123")) {
                employeeWindow = new EmployeeView();
            }
        }
    }

    class AddAdB1Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.importProducts();
        }
    }
    class AddAdB2Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.addProduct(adminWindow.getFirst(), adminWindow.getSecond(), adminWindow.getThird(), adminWindow.getFourth(), adminWindow.getFifth(), adminWindow.getSixth(), adminWindow.getSeventh());
        }
    }
    class AddAdB3Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.modifyProduct(adminWindow.getFirst(), adminWindow.getSecond(), adminWindow.getThird(), adminWindow.getFourth(), adminWindow.getFifth(), adminWindow.getSixth(), adminWindow.getSeventh());
        }
    }
    class AddAdB4Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.deleteProduct(adminWindow.getFirst());
        }
    }
    class AddAdB5Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.addToCompositeProduct(adminWindow.getFirst(),adminWindow.getEighth());
        }
    }
    class AddAdB6Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            int time1 = 10;
            int time2 = 20;
            List<Order> list1 = d.getKeys().stream().filter(i -> i.getDate().getHour() >= time1 && i.getDate().getHour() <= time2).collect(Collectors.toList());
            fw.createReport1((ArrayList<Order>) list1, time1, time2);

            int minOrder = 1;
            List<MenuItem> list2 = d.getItems().stream().filter(i -> i.getNoOfOrders() > minOrder).collect(Collectors.toList());
            fw.createReport2((ArrayList<MenuItem>) list2, minOrder);

            int minOrders = 1;
            int minPrice = 10;
            ArrayList<Client> clients = new ArrayList<>();
            Map<Integer, Long> map = d.getKeys().stream().filter(i -> i.getPrice() > minPrice).collect(Collectors.groupingBy(j -> j.getClientId(), Collectors.counting()));
            ArrayList<Client> list3 = d.getClients();
            if(map != null) {
                for (Client k : list3) {
                    if (map.get(k.getClientId()) != null && map.get(k.getClientId()) > minOrders)
                        clients.add(k);
                }
                fw.createReport3(clients, minOrders, minPrice);
            }

            int day = 28;
            ArrayList<MenuItem> fullList = new ArrayList<>();
            List<Order> list4 = d.getKeys().stream().filter(i -> i.getDate().getDayOfMonth() == day).collect(Collectors.toList());
            for(Order k : list4){
                ArrayList<MenuItem> items = d.getOrderedItems(k);
                fullList.addAll(items);
            }
            fw.createReport4(fullList,day);
        }
    }

    class AddAdB7Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            Serializator.serializator(d);
        }
    }

    class AddClB1Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> items = d.findProducts(clientWindow.getFirst(),clientWindow.getSecond(),clientWindow.getThird(),clientWindow.getFourth(),clientWindow.getFifth(),clientWindow.getSixth(),clientWindow.getSeventh());
            clientWindow.showProducts(items);
        }
    }
    class AddClB2Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.addItemToOrder(clientId, clientWindow.getFirst());
            clientWindow.setEighth(d.getClient(clientId).getList());
        }
    }
    class AddClB3Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            d.placeOrder(clientId);
            Order o = d.getOrder(clientId);
            FileWriter fw = new FileWriter();
            fw.createBill(o,d.getOrderedItems(o));
            if(employeeWindow != null){
                employeeWindow.show(d.getClient(clientId).getList(),clientId,o.getOrderId());
            }
            d.getClient(clientId).newOrderList();
            clientWindow.setEighth(d.getClient(clientId).getList());
        }
    }
    class AddClB4Listener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> items = d.getItems();
            clientWindow.showProducts(items);
        }
    }
}
