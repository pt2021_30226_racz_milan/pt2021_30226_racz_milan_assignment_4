package presentation;

import business.MenuItem;

import java.util.ArrayList;

public interface Observer {

    void show(ArrayList<MenuItem> items, int clientId, int orderId);

}
