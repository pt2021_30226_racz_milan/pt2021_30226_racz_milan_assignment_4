package presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class View {
    private final JLabel l1 = new JLabel ("Administrator: ");
    private final JLabel l2 = new JLabel ("Client: ");
    private final JLabel l3 = new JLabel ("Userame: ");
    private final JLabel l4 = new JLabel ("Password: ");
    private final JLabel l5 = new JLabel ("Employee: ");
    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);
    private JButton b1 = new JButton("Enter as administrator");
    private JButton b2 = new JButton("Login");
    private JButton b3 = new JButton("Register");
    private JButton b4 = new JButton("Enter as employee");

    public View(){
        JFrame frame = new JFrame ("Login");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(230, 320);

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();
        JPanel panel7 = new JPanel();
        JPanel panel8 = new JPanel();
        JPanel panel9 = new JPanel();

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(0,1,0,3);

        panel.setLayout(flLayout);
        panel1.setLayout(flLayout);
        panel2.setLayout(flLayout);
        panel3.setLayout(flLayout);
        panel4.setLayout(grLayout);
        panel5.setLayout(grLayout);
        panel6.setLayout(flLayout);
        panel7.setLayout(flLayout);
        panel8.setLayout(flLayout);
        panel9.setLayout(flLayout);

        panel1.add(l1);
        panel2.add(b1);
        panel3.add(l2);

        panel4.add(l3);
        panel4.add(l4);
        panel5.add(tf1);
        panel5.add(tf2);

        panel6.add(panel4);
        panel6.add(panel5);

        panel7.add(b2);
        panel7.add(b3);

        panel8.add(l5);

        panel9.add(b4);

        panel.add(panel1);
        panel.add(panel2);
        panel.add(panel3);
        panel.add(panel6);
        panel.add(panel7);
        panel.add(panel8);
        panel.add(panel9);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }

    void addB1Listener(ActionListener s) {
        b1.addActionListener(s);
    }
    void addB2Listener(ActionListener s) {
        b2.addActionListener(s);
    }
    void addB3Listener(ActionListener s) {
        b3.addActionListener(s);
    }
    void addB4Listener(ActionListener s) {
        b4.addActionListener(s);
    }
}
